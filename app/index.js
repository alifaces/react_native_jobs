import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, TouchableOpacity, ActivityIndicator, Image, TextInput } from 'react-native';
import axios from 'axios';

function App() {
  const [meals, setMeals] = useState([]);
  const [selectedMeal, setSelectedMeal] = useState(null);
  const [searchQuery, setSearchQuery] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    fetchMeals();
  }, []);

  const fetchMeals = async () => {
    try {
      setIsLoading(true);
      const response = await axios.get(
        'https://www.themealdb.com/api/json/v1/1/filter.php?c=Dessert',
      );
      const filteredMeals = response.data.meals.filter((meal) => {
        return meal.strMeal !== null && meal.strMeal !== '';
      });
      const sortedMeals = filteredMeals.sort((a, b) =>
        a.strMeal.localeCompare(b.strMeal),
      );
      const mealsWithImages = await Promise.all(sortedMeals.map(async (meal) => {
        const response = await axios.get(`https://www.themealdb.com/api/json/v1/1/lookup.php?i=${meal.idMeal}`);
        const mealWithImage = {
          ...meal,
          imageUrl: response.data.meals[0].strMealThumb
        };
        return mealWithImage;
      }));
      setMeals(mealsWithImages);
      setIsLoading(false);
    } catch (error) {
      console.error(error);
      setIsLoading(false);
    }
  };

  const filterMeals = (meals) => {
    return meals.filter((meal) => {
      return meal.strMeal.toLowerCase().includes(searchQuery.toLowerCase());
    });
  };

  const renderMealItem = ({ item }) => (
    <TouchableOpacity onPress={() => fetchMealDetails(item.idMeal)}>
      <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 5 }}>
        <Image source={{ uri: item.imageUrl }} style={{ width: 50, height: 50, marginRight: 10 }} />
        <Text style={{ fontSize: 18 }}>{item.strMeal}</Text>
      </View>
    </TouchableOpacity>
  );

  const fetchMealDetails = async (mealId) => {
    try {
      setIsLoading(true);
      const response = await axios.get(
        `https://www.themealdb.com/api/json/v1/1/lookup.php?i=${mealId}`,
      );
      const meal = response.data.meals[0];
      setSelectedMeal(meal);
      setIsLoading(false);
    } catch (error) {
      console.error(error);
      setIsLoading(false);
    }
  };

  const renderMealDetails = () => {
    if (selectedMeal) {
      const ingredients = [];
      for (let i = 1; i <= 20; i++) {
        if (
          selectedMeal[`strIngredient${i}`] !== '' &&
          selectedMeal[`strIngredient${i}`] !== null
        ) {
          ingredients.push(
            `${selectedMeal[`strMeasure${i}`]} ${
              selectedMeal[`strIngredient${i}`]
            }`,
          );
        }
      }
      return (
        <View style={{ padding: 10 }}>
          <Image source={{ uri: selectedMeal.imageUrl }} style={{ width: '100%', height: 200, marginBottom: 10 }} />
          <Text style={{ fontSize: 24, fontWeight: 'bold' }}>
            {selectedMeal.strMeal}
          </Text>
          <Text style={{ fontSize: 18, marginTop: 10 }}>
            {selectedMeal.strInstructions}
          </Text>
          <Text style={{ fontSize: 18, marginTop: 10, fontWeight: 'bold' }}>
            Ingredients:
          </Text>
          {ingredients.map((ingredient, index) => (
            <Text key={index} style={{ fontSize: 16, marginTop: 5 }}>
              {ingredient}
            </Text>
          ))}
        </View>
      );
    } else {
      return null;
    }
  };

  const renderSearchBar = () => {
    return (
      <TextInput
        style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
        onChangeText={(text) => setSearchQuery(text)}
        value={searchQuery}
        placeholder="Search for a meal..."
      />
    );
  };

  const renderSpinner = () => {
    if (isLoading) {
      return <ActivityIndicator />;
    } else {
      return null;
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <View style={{ padding: 10 }}>
        <TextInput
          style={{ height: 40, borderColor: 'gray', borderWidth: 1, marginBottom: 10 }}
          onChangeText={(text) => setSearchQuery(text)}
          value={searchQuery}
          placeholder="Search desserts..."
        />
        {isLoading ? (
          <ActivityIndicator size="large" color="#0000ff" />
        ) : (
          <FlatList
            data={filterMeals(meals)}
            renderItem={renderMealItem}
            keyExtractor={(item) => item.idMeal}
          />
        )}
      </View>
      {renderMealDetails()}
    </View>
  );
}

export default App;
